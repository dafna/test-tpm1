#include <stdio.h>
#include <unistd.h>
#include "tpm.h"
#include "priv_tpm.h"

#define TPM_ORD_GET_RANDOM 70
#define TPM_ORD_GET_TEST_RESULT 84
#define TPM_ORD_GET_CAP 101
#define TPM_TAG_RQU_COMMAND 193

#define TPM_TAG_RSP_COMMAND 0x00C4

#define TPM_TAG_CAP_VERSION_INFO 0x0030 //structure TPM_CAP_VERSION_INFO

#define RES_BUF_SZ 4096
/*
 * This code tests a tpm 1.2 when it is
 * on 'failure mode'.
 * This is the expected behaviour:
 * 1. the get_result command should succeed
 * 2. the get_capability command should succeed for a limitied list of capabilites
 * 3. the init_tpm might succeed.
 * 4. any other command should fail (I will test the get_random command
 */

/* do standart checks and return the payload length */
int standart_cmd_check(struct tpm_buf buf)
{
	struct tpm_header *resp_header;
	unsigned int result, len;

	resp_header = (struct tpm_header*) buf.data;
	result = __be32_to_cpu(resp_header->return_code);
	printf("response length: %d\n", tpm_buf_length(&buf));
	printf("response tag:    0x%x\n", tpm_buf_tag(&buf));
	printf("response result: %d\n", result);
	if (result != TPM2_RC_SUCCESS) {
		printf("no success %u != %u\n", result, TPM2_RC_SUCCESS);
		return -1;
	}
	if (tpm_buf_tag(&buf) != TPM_TAG_RSP_COMMAND) {
		printf("wrong tag, %u != %u\n", tpm_buf_tag(&buf), TPM_TAG_RSP_COMMAND);
		return -1;
	}

	len = __be32_to_cpu(buf.data[TPM_HEADER_SIZE]);
	printf("response len:    %d\n", len);
	return len;
}

/* 1. the get_result command should succeed */
int test_get_test_result(int fd) {

	struct tpm_buf buf;
	char *res;
	int len, rc;

	rc = tpm_buf_init(&buf, TPM_TAG_RQU_COMMAND, TPM_ORD_GET_TEST_RESULT);

	if (rc) {
		printf("failed to init buf\n");
		return rc;
	}
	rc = write(fd, buf.data, tpm_buf_length(&buf));
	if (rc <= 0)
		goto out;
	rc = read(fd, buf.data, RES_BUF_SZ);
	if (rc <= 0)
		goto out;
	rc = len = standart_cmd_check(buf);
	if (len < 0)
		goto out;

	res = &buf.data[TPM_HEADER_SIZE + 4];
	for (int i = 0; i < len && i < 1000; i++)
		printf("%c ", res[i]);
	printf("\n");

	tpm_buf_destroy(&buf);
	return 0;
out:
	printf("== FAILURE == %d\n", rc);
	tpm_buf_destroy(&buf);
	return rc;
}


/* 2. the get_capability command should succeed for a limitied list of capabilites
 * If the TPM is in failure mode or limited operation mode, the TPM MUST return
 * a. TPM_CAP_VERSION
 * b. TPM_CAP_VERSION_VAL
 * c. TPM_CAP_MFR
 * d. TPM_CAP_PROPERTY -> TPM_CAP_PROP_MANUFACTURER
 * e. TPM_CAP_PROPERTY -> TPM_CAP_PROP_DURATION
 * f. TPM_CAP_PROPERTY -> TPM_CAP_PROP_TIS_TIMEOUT
 */

int test_get_capability_version_1_2(int fd) {
	struct tpm_buf buf;
	struct tpm1_version2 version2;
	int len, rc;

	rc = tpm_buf_init(&buf, TPM_TAG_RQU_COMMAND, TPM_ORD_GET_CAP);
	if (rc) {
		printf("failed to init buf\n");
		return rc;
	}

	tpm_buf_append_u32(&buf, TPM_CAP_VERSION_1_2);
	tpm_buf_append_u32(&buf, 0);
	rc = write(fd, buf.data, tpm_buf_length(&buf));
	if (rc <= 0)
		goto out;
	rc = read(fd, buf.data, RES_BUF_SZ);
	if (rc <= 0)
		goto out;
	rc = len = standart_cmd_check(buf);
	if (len < 0)
		goto out;

	version2 = *(struct tpm1_version2 *)&buf.data[TPM_HEADER_SIZE + 4];
	printf("tag is %u\n", __be16_to_cpu(version2.tag));
	printf("major is %u\n", version2.version.major);
	printf("manor is %u\n", version2.version.minor);
	printf("rev_major is %u\n", version2.version.rev_major);
	printf("rev_minor is %u\n", version2.version.rev_minor);

	tpm_buf_destroy(&buf);
	return 0;
out:
	printf("== FAILURE == %d\n", rc);
	tpm_buf_destroy(&buf);
	return rc;

}

/* the 'get random' command should fail when the tpm is in failure mode */
int test_get_random(int fd)
{
#define NM_BYTES 100
	struct tpm_buf buf;
	struct tpm_header *resp_header;
	unsigned int result;
	int rc;

	rc = tpm_buf_init(&buf, TPM_TAG_RQU_COMMAND, TPM_ORD_GET_RANDOM);
	if (rc) {
		printf("failed to init buf\n");
		return rc;
	}
	tpm_buf_append_u32(&buf, NM_BYTES);
	rc = write(fd, buf.data, tpm_buf_length(&buf));
	if (rc <= 0)
		goto out;
	rc = read(fd, buf.data, RES_BUF_SZ);
	if (rc <= 0)
		goto out;
	resp_header = (struct tpm_header*) buf.data;
	result = __be32_to_cpu(resp_header->return_code);
	printf("response length: %d\n", tpm_buf_length(&buf));
	printf("response tag:    0x%x\n", tpm_buf_tag(&buf));
	printf("response result: %d\n", result);
	if (result == TPM2_RC_SUCCESS) {
		printf("get random succeeded although it should have failed!\n");
		rc = -1;
		goto out;
	}

	tpm_buf_destroy(&buf);
	return 0;
out:
	printf("== FAILURE == %d\n", rc);
	tpm_buf_destroy(&buf);
	return rc;


}

int main()
{
	int rc, fd;

	fd = open("/dev/tpm0", O_RDWR);
	if (fd < 0) {
		printf("failed to open file\n");
		return fd;
	}
	printf("TEST get_test_result\n");
	rc = test_get_test_result(fd);
	printf("\nTEST test_get_capability_version_1_2\n");
	rc = test_get_capability_version_1_2(fd);
	printf("\nTEST test_get_random\n");
	rc = test_get_random(fd);
	return rc;
}
