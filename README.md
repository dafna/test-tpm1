A tiny test for TPM 1.2, for the case that
the '/dev/tpm0' node is available while the
tpm is in failure mode

compile:
```
gcc test-tpm1-failure-mode.c
```

3 tests run:
1. make sure that the command TPM_GetTestResult succeed
2. make sure that the command TPM_GetCapability for capArea 'TPM_CAP_VERSION_VAL' succeed
2. make sure that the command TPM_GetRandom fails
